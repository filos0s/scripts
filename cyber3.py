#! /usr/bin/python
import re
import urllib2
from urllib2 import urlopen
from bs4 import BeautifulSoup
import sys

_REQ= urllib2.Request('http://cybercrime-tracker.net/index.php?s=0&m=900000', headers={'User-Agent' : "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0"})
_DATA=BeautifulSoup(urlopen(_REQ).read())
_INFO=_DATA('table')[0]
_ROWS=_INFO.findAll('tr')
res=dict()
res['source']='http://cybercrime-tracker.net/'
res['task']='domain_insertion'
for _ROW in _ROWS:
	i=0
        
	for _FILAS in _ROW.findAll('td'):
		i=i+1
		if i == 1: 
			_DATE =_FILAS.get_text()#+" 00:00:00"
			dd=_DATE.split('-')[0]
			mm=_DATE.split('-')[1]
			yy=_DATE[6:]
			_DATE=yy+'-'+mm+'-'+dd+" 00:00:00"
			res['date']=_DATE.encode('ascii','ignore')
		
		if i == 2:
			_URL=_FILAS.get_text()
			res['urls']=[_URL.encode('ascii','ignore')]
			_DOMAIN =_URL.split('/')[0]			
				
		if i == 3:
			_IP=_FILAS.get_text()
			if ':' in _IP :
				 _IP=_IP.split(':')[0]
		
		if i == 4:
			_DES=_FILAS.get_text()
			res['description']=_DES.encode('ascii','ignore')
			
			if _DES != "" and _DES != '-::TYPE':
			
				if re.match('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', _DOMAIN) != None:
					res3=dict()
					res3['source']='http://cybercrime-tracker.net/'
					res3['date']=_DATE.encode('ascii','ignore')
					res3['task']='ip_insertion' 
					if ':' in _DOMAIN :
                               			_DOMAIN=_DOMAIN.split(':')[0]
					res3['ip']=_DOMAIN.encode('ascii','ignore')
					res3['description']=_DES.encode('ascii','ignore')
					print res3 
					
				else:
					if ':' in _DOMAIN: continue  
					if '(' in _DOMAIN: continue
					if len(_IP) < 7 :
						res4=dict()
						res4['description']=_DES.encode('ascii','ignore')
						res4['urls']=[_URL.encode('ascii','ignore')]
						res4['date']=_DATE.encode('ascii','ignore')
						res4['domain']=_DOMAIN.encode('ascii','ignore')
						res4['source']='http://cybercrime-tracker.net/'
						res4['task']='domain_insertion'    	                                  	
						print res4
			
				if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",_IP) != None:
					
					res2=dict()
					res2['description']=_DES.encode('ascii','ignore')
					res2['source']='http://cybercrime-tracker.net/'
					res2['date']=_DATE.encode('ascii','ignore')
					res2['task']='ip_insertion' 
					res2['ip']=_IP.encode('ascii','ignore')
					res['resolved_ip']=_IP.encode('ascii','ignore')
					res['domain']=_DOMAIN.encode('ascii','ignore')
					print res
					print res2
				
					
