#!/usr/bin/python

import urllib2

_URL = "http://www.kleissner.org/text/ZeuSGameover_Domains.txt"
req = urllib2.Request (_URL, headers= {'User-Agent' : "Magic Browser"})
data = urllib2.urlopen(req).read()

_DESCRIPTION="Zeus DGA domain"
_SOURCE="kleissner.org"
_TASK="domain_insertion"

res = dict ()
res['task'] = _TASK 
res['description'] = _DESCRIPTION
res['source'] = _SOURCE

domain_list = data.split(',')

for domain in domain_list:
	if "Date" in domain: continue
	if "Domain" in domain: continue
	res['domain'] = domain.split('\r\n') [0]
	print res

