#! /usr/bin/python
import sys
from dns import flags, resolver

try:
    server = (str(sys.argv[1]))
    answer = resolver.query(server)

    dnsflags = (flags._to_text(answer.response.flags,flags._by_value,flags._flags_order)).split(" ")
    if "RA" in dnsflags:
        print "El Servidor:"+server+" es aparentemente vulnerable "

    else:
        print "No recursivo"
except:
    print "La cagaste"

