import subprocess
import threading
import ast
import ipcalc
import re
from urllib2 import urlopen
from bs4 import BeautifulSoup
import sys

class gcb_update():
    def __init__(self,instruction_list,threads):
        self.resolver=['whois.radb.net','whois.arin.net', 'whois.ripe.net', 'whois.apnic.net', 'whois.lacnic.net', 'whois.afrinic.net']
        self.threads=threads
        self.ip_block=[]
        self.out=[]
        self.instruction_list=instruction_list
        self.multiThreading();
#######################################################################
##### Returns a dictionary with, ip, action and result from ping ######
#######################################################################
    def performIpPing(self,ip_address):
        p = subprocess.Popen("ping -c 1 -w 1 "+ip_address,shell=True, stdout=subprocess.PIPE)
        d={}
        while p.poll() is None:
            p.stdout.readline()
        if p.returncode==0:
            d={"ip":ip_address, "action" : "ping" , "result": "True"}
        else:
            d={"ip":ip_address, "action" : "ping" , "result": "False"}
        return d
#######################################################################
##### Returns a dictionary with, domain, action and              ######
##### result from ping                                           ######
#######################################################################
    def performDomainPing(self, domain):
        p = subprocess.Popen("ping -c 1 -w 1 "+domain,shell=True, stdout=subprocess.PIPE)
        d={}
        while p.poll() is None:
            p.stdout.readline()
        if p.returncode==0:
            d={"domain":domain, "action" : "ping" , "result": "True"}
        else:
            d={"domain":domain, "action" : "ping" , "result": "False"}
        return d
#######################################################################
##### Instruction parameter is a dictionary and it              #######
##### should contain action, ip, and parameters                 #######
##### This function returns a dictionary with the fields:       #######
##### action and port:value, where value is True if it open     #######
##### else False if it closed                                   #######
##### This function control the action to be taken depending    #######
##### on the parameters                                         #######
#######################################################################
    def performIpNmap(self,instruction):
        read_list=[]
        p = subprocess.Popen("nmap "+instruction["parameters"]+" "+instruction["ip"],shell=True, stdout=subprocess.PIPE)
        while p.poll() is None:
            l = p.stdout.readline()
            if l!='\n':
                read_list.append(str(l).replace("\n","",3))
        if(instruction["parameters"]==""):
            return self.simpleIpNmap(read_list,instruction)
#######################################################################
##### This is the parser for a simple nmap instruction          #######
##### where dictionary field "parameters" has the value ""      #######
#######################################################################
    def simpleIpNmap(self,read_list,instruction):
        ports=[]
        nmap={"ip" : instruction["ip"] , "action" : "ipnmap" }
        position=0
        string=""
        clean=""
        state=""
        for i in range(0,len(read_list)):
            if "PORT" in read_list[i]:
                position=i+1
        for i in range(position,len(read_list)-2):
            number=read_list[i].split("/")[0]
            string= str(read_list[i]).split(" ")
            for s in string:
                if s=="open":
                    state="True"
                    break
                elif s=="closed":
                    state="False"
                    break
            if number!="None" and number!=None:
                nmap[str(number)]=str(state)
        return nmap
#######################################################################
##### Instruction parameter is a dictionary and it              #######
##### should contain action, domain, and parameters                 #######
##### This function returns a dictionary with the fields:       #######
##### action and port:value, where value is True if it open     #######
##### else False if it closed                                   #######
##### This function control the action to be taken depending    #######
##### on the parameters                                         #######
#######################################################################
    def performDomainNmap(self,instruction):
        read_list=[]
        p = subprocess.Popen("nmap "+instruction["parameters"]+" "+instruction["domain"],shell=True, stdout=subprocess.PIPE)
        while p.poll() is None:
            l = p.stdout.readline()
            if l!='\n':
                read_list.append(str(l).replace("\n","",3))
        if(instruction["parameters"]==""):
            return self.simpleDomainNmap(read_list,instruction)
#######################################################################
##### This is the parser for a simple nmap instruction          #######
##### where dictionary field "parameters" has the value ""      #######
#######################################################################
    def simpleDomainNmap(self,read_list,instruction):
        ports=[]
        nmap={"domain" : instruction["domain"] , "action" : "domainnmap" }
        position=0
        string=""
        clean=""
        state=""
        for i in range(0,len(read_list)):
            if "PORT" in read_list[i]:
                position=i+1
        for i in range(position,len(read_list)-2):
            number=read_list[i].split("/")[0]
            string= str(read_list[i]).split(" ")
            for s in string:
                if s=="open":
                    state="True"
                    break
                elif s=="closed":
                    state="False"
                    break
            if number!="None" and number!=None:
                nmap[str(number)]=str(state)
        return nmap
#######################################################################
##### This function is where threads are managed                #######
#####                                                           #######
#######################################################################
    def multiThreading(self):
        threads=[]
        for i in range(0,self.threads):
            t=threading.Thread(target=self.toDo)
            t.start()
            threads.append(t)
        for thread in threads:
            thread.join()
#######################################################################
##### Atomic function for IPwhois                               #######
##### return                                                    #######
#######################################################################

    def performIpWhois(self,ip,action):
        try:
            d={}
            read_list=[]
            header=[]
            result=[]
            for r in self.resolver:
                if not read_list:
                    p = subprocess.Popen("whois -h whois.cymru.com '   -v "+ip+"'",shell=True, stdout=subprocess.PIPE)  
                    while p.poll() is None:
                        l = p.stdout.readline()
                        if l!='\n' and l!="":
                            read_list.append(str(l).replace(" ","").replace("\n","").split("|"))
                    header= read_list[0]
                    result= read_list[1]
                    for i in range(0,len(header)):
                        if(header[i]!="ip" and result[i]!=""):
                            d[header[i].lower()]=result[i]
                    d["action"]=action;
            return d
        except Exception,e:
             print str(e)
#######################################################################
##### this function returns a dictionary whith result of calucale #####
##### subnets from IP asn                                         #####
#######################################################################
    def IpAsnCalc(self, instruction):
        whois=self.performIpWhois(instruction['ip'],instruction["action"])
        asn=str(whois['as'])
        subnets=self.asnBlocks(asn)
        ip_list=self.calcBlocks(subnets)
        out={'action:': instruction['action'], 'ip' : instruction['ip'], 'subnets':subnets,'number_of_ips':str(len(ip_list)), 'bgp':whois['bgpprefix'],'as_name': whois['asname'] }
        return out
#######################################################################
##### this function returns a dictionary whith result of calucale #####
##### subnets from IP asn                                         #####
#######################################################################
    def DomainAsnCalc(self, instruction):
        whois=self.performDomainWhois(instruction['domain'])
        asn=str(whois['as'])
        subnets=self.asnBlocks(asn)
        ip_list=self.calcBlocks(subnets)
        out={ 'domain':whois['domain'],'action:': instruction['action'], 'ip' : whois['ip'], 'subnets':subnets,'number_of_ips':str(len(ip_list)), 'bgp':whois['bgpprefix'],'as_name': whois['asname'] }
        return out

#######################################################################
##### This function returns a list with the subnets associated  #######
##### with the IP ASN                                           #######
#######################################################################
    def asnBlocks(self,asn):
        read_list=[]
        for r in self.resolver:
            if not read_list:
                #print r
                bash="whois -h "+r+" -i origin -T route AS" + str(asn) + " | grep -w 'route:' | awk '{print $NF}' |sort -n "
                p = subprocess.Popen(bash,shell=True, stdout=subprocess.PIPE)
                while p.poll() is None:
                    l = str(p.stdout.readline()).replace("\n","")
                    if l!=" " and l!="\n" and l!="" and l!=None:
                        read_list.append(l)
            else:
                return read_list
#######################################################################
##### This function calculate Ip addresses from a subnet         ######
#####                                                            ######
#######################################################################
    def calcBlocks(self,subnet_list):
        result=[]
        for ip in subnet_list:
            for sub in ipcalc.Network(ip):
                result.append(str(sub))
        return result

#######################################################################
#####  This function execute a domain whois                      ######
#######################################################################    
    def performDomainWhois(self,domain):
        d={}
        
        p = subprocess.Popen("host "+domain,shell=True, stdout=subprocess.PIPE)
        line=str(p.stdout.readline())
        if line.find("has address") >=0:
            ip=line.split(" ")[3];
            d=self.performIpWhois(ip,"domainwhois")  
            d["domain"]=domain    
        return d
    def performIpVoidQuery(self,ip_address):
        try:
            _IP=(str(ip_address))
            _DATA = BeautifulSoup(urlopen('http://www.ipvoid.com/scan/'+_IP).read())
            _INFO=_DATA('table')[0]
            _BL=_DATA('table')[1]
            rows=_INFO.findAll('tr')
            filas=_BL.findAll('tr')
            x=0
            res=dict()
            res['records']=dict()

################## PARSE DE INFO ##########################

            for roW in rows:
                x=x+1
                if x == 3:
                    _IP=roW.get_text()
                    _IP=_IP[10:].split(' ')[0].encode('ascii','ignore')
                    res['ip']=_IP
                if x == 4:
                    _RDNS=roW.get_text()
                    _RDNS=_RDNS[11:].encode('ascii','ignore')
                    if _RDNS != 'Unknown':
                        res['reverse_dns']=_RDNS
                if x == 5:
                    _ASN=roW.get_text()
                    _ASN=_ASN[3:].encode('ascii','ignore')
                    _ASN = re.sub("\D", "", _ASN)
                    if _ASN != 'Unknown':
                        res['asn']=_ASN
                if x == 6:
                    _ASNOW=roW.get_text()
                    _ASNOW=_ASNOW[9:].encode('ascii','ignore')
                    if _ASNOW != 'Unknown':
                        res['asn_owner']=_ASNOW
                if x == 7:
                    _ISP=roW.get_text()
                    _ISP=_ISP[3:].encode('ascii','ignore')
                    if _ISP != 'Unknown':
                        res['isp']=_ISP
                if x == 9:
                    _CC=roW.get_text()
                    _CC=_CC[14:].encode('ascii','ignore').split(')')[0]
                    if _CC!= 'Unknown':
                        res['country_code']=_CC
                if x == 11:
                    _CITY=roW.get_text()
                    _CITY=_CITY[4:].encode('ascii','ignore')
                    if _CITY != 'Unknown':
                        res['city']=_CITY
                if x == 12:
                    _RON=roW.get_text()
                    _RON=_RON[6:].encode('ascii','ignore')
                    if _RON != 'Unknown ':
                        res['region']=_RON

################## PARSE DE BL ##########################

            for campo in filas :
                name= campo.get_text()
                if 'Engine' in name: continue
                name=name[1:]
                if name[0] != ' ':
                    for link in campo.findAll('a'):
                        hrf=link.get('href').encode('ascii','ignore')
                        name=name.split(' ')[0].encode('ascii','ignore')
                        if name != ' ':
                            res['records'][name]=hrf
            if res['records'] != {}:
                res['action']='ipvoidquery'
                res['ip']=ip_address
                return res

        except:
                pass

    def performUrlVoidQuery(self,domain):
        try:
            _DOMAIN=(str(domain))
            _DATA = BeautifulSoup(urlopen('http://www.urlvoid.com/scan/'+_DOMAIN).read())
            _INFO=_DATA('tbody')[2]
            _BL=_DATA('table')[1]
            rows=_INFO.findAll('tr')
            filas=_BL.findAll('tr')
            x=0
            res=dict()
            res['records']=dict()
################## PARSE DE INFO ##########################
            for roW in rows:
                x=x+1
                if x == 1:
                    _IP=roW.get_text()
                    _IP=_IP[10:].split(' ')[0].encode('ascii','ignore')
                    res['resolved ip']=_IP
                    res['domain']=_DOMAIN
                if x == 3:
                    _ASN=roW.get_text()
                    _ASN=_ASN[3:].encode('ascii','ignore')
                    _ASN = re.sub("\D", "", _ASN)
                    if _ASN != 'Unknown':
                        res['asn']=_ASN
                if x == 4:
                    _ASNOW=roW.get_text()
                    _ASNOW=_ASNOW[9:].encode('ascii','ignore')
                    if _ASNOW != 'Unknown':
                        res['asn_owner']=_ASNOW

                if x == 6:
                    _CC=roW.get_text()
                    _CC=_CC[14:].encode('ascii','ignore').split(')')[0]
                    if _CC!= 'Unknown':
                        res['country_code']=_CC
                if x == 8:
                    _CITY=roW.get_text()
                    _CITY=_CITY[4:].encode('ascii','ignore')
                    if _CITY != 'Unknown':
                        res['city']=_CITY
                if x == 9:
                    _RON=roW.get_text()
                    _RON=_RON[6:].encode('ascii','ignore')
                    if _RON != 'Unknown ':
                        res['region']=_RON

################## PARSE DE BL ##########################

            for campo in filas :
                name= campo.get_text()
                if 'Engine' in name: continue
                name=name[1:]
                if name[0] != ' ':
                    for link in campo.findAll('a'):
                        hrf=link.get('href').encode('ascii','ignore')
                        name=name.split(' ')[0].encode('ascii','ignore')
                        if name != ' ':
                            res['records'][name]=hrf
            if res['records'] != {}:
                res['action']='urlvoidquery'
                res['domain']=str(domain)
                return res
        except Exception,e:
             print str(e)

#######################################################################
##### This function controls the actions that will gcb perform   ######
##### depending the value of "action" key                        ######
##### The result will be append in a list named "out"            ######
#######################################################################
    def toDo(self):
        while len(self.instruction_list)>0:
            instruction=self.instruction_list.pop()
            if instruction["action"]=="ipping":
                self.out.append(self.performIpPing(instruction["ip"]))
            elif instruction["action"]=="ipnmap":
                self.out.append(self.performIpNmap(instruction))
            elif instruction["action"]=="domainnmap":
                self.out.append(self.performDomainNmap(instruction))
            elif instruction["action"]=="ipasncalc":
                self.out.append(self.IpAsnCalc(instruction))
            elif instruction["action"]=="domainasncalc":
                self.out.append(self.DomainAsnCalc(instruction))
            elif instruction["action"]=="domainping":
                self.out.append(self.performDomainPing(instruction["domain"]))
            elif instruction["action"]=="ipwhois":
                self.out.append(self.performIpWhois(instruction["ip"],instruction["action"]))
            elif instruction["action"]=="domainwhois":
                self.out.append(self.performDomainWhois(instruction["domain"]))
            elif instruction["action"]=="ipvoidquery":
                self.out.append(self.performIpVoidQuery(instruction["ip"]))
            elif instruction["action"]=="urlvoidquery":
                self.out.append(self.performUrlVoidQuery(instruction["domain"]))


instruction_list=[
{'action': 'ipping',        'ip': '8.8.8.8',            'parameters':''},
{'action': 'ipping',        'ip': '161.60.66.6',        'parameters':''},
{'action': 'ipnmap',        'ip': '127.0.0.1',          'parameters':''},
{'action': 'ipasncalc',     'ip': '8.8.8.8',            'parameters':''},
{'action': 'ipwhois',       'ip': '54.217.215.139',     'parameters':''},
{'action': 'ipvoidquery',   'ip': '6.6.6.6',            'parameters':''},

{'action': 'domainwhois',   'domain': 'xataka.com.mx',  'parameters':''},
{'action': 'domainping',    'domain': '8.8.8.8',        'parameters':''},
{'action': 'domainnmap',    'domain': 'facebook.com',   'parameters':''},
{'action': 'urlvoidquery',  'domain': 'google.com',     'parameters':''},
{'action': 'domainasncalc', 'domain': 'facebook.com',   'parameters':''}

]
obj=gcb_update(instruction_list,200)
print "////////////////////////////////////////////////////////////////////////////////////////////////////////////////"
for res in obj.out:
    print ">"+str(res)+"\n"

