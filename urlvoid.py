#! /usr/bin/python

import re
from urllib2 import urlopen
from bs4 import BeautifulSoup
import sys

try:
    _DOMAIN=(str(sys.argv[1]))
    _DATA = BeautifulSoup(urlopen('http://www.urlvoid.com/scan/'+_DOMAIN).read())
    _INFO=_DATA('tbody')[2]
    _BL=_DATA('table')[1]
    rows=_INFO.findAll('tr')
    filas=_BL.findAll('tr')
    x=0
    res=dict()
    res['records']=dict()
################## PARSE DE INFO ##########################
    for roW in rows:
        x=x+1
        if x == 1:
            _IP=roW.get_text()
            _IP=_IP[10:].split(' ')[0].encode('ascii','ignore')
            res['resolved ip']=_IP
            res['domain']=_DOMAIN
        if x == 3:
            _ASN=roW.get_text()
            _ASN=_ASN[3:].encode('ascii','ignore')
            _ASN = re.sub("\D", "", _ASN)
            if _ASN != 'Unknown':
                res['asn']=_ASN
        if x == 4:
            _ASNOW=roW.get_text()
            _ASNOW=_ASNOW[9:].encode('ascii','ignore')
            if _ASNOW != 'Unknown':
                res['asn_owner']=_ASNOW

        if x == 6:
            _CC=roW.get_text()
            _CC=_CC[14:].encode('ascii','ignore').split(')')[0]
            if _CC!= 'Unknown':
                res['country_code']=_CC
        if x == 8:
            _CITY=roW.get_text()
            _CITY=_CITY[4:].encode('ascii','ignore')
            if _CITY != 'Unknown':
                res['city']=_CITY
        if x == 9:
            _RON=roW.get_text()
            _RON=_RON[6:].encode('ascii','ignore')
            if _RON != 'Unknown ':
                res['region']=_RON

################## PARSE DE BL ##########################

    for campo in filas :
        name= campo.get_text()
        if 'Engine' in name: continue
        name=name[1:]
        if name[0] != ' ':
            for link in campo.findAll('a'):
                hrf=link.get('href').encode('ascii','ignore')
                name=name.split(' ')[0].encode('ascii','ignore')
                if name != ' ':
                    res['records'][name]=hrf
    if res['records'] != {}:
        print res




except Exception,e:
     print str(e)



